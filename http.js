const http = require('http');
const fs = require('fs');

const server = http.createServer((req, res)=>{
    let file = __dirname +req.url;
   fs.readFile(file , (err, data)=>{
       if(err){
           
           res.write("File Not Found")
       }
       else{
           res.write(data);
       }
       res.end();
   })
   

});

server.listen(2000);
console.log('listen on port 3000');
